package th.ac.tu.siit.lab6contactlist;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.*;
import android.widget.*;

public class AddNewActivity extends Activity {

	int position;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		
		//check if the activity is started with extras
		Intent i = this.getIntent();
		if(i.hasExtra("pos")){
			//Editing mode
			//Load the extras
			String name = i.getStringExtra("name");
			String phone = i.getStringExtra("phone");
			
			String mail = i.getStringExtra("email");
			
			int phoneType = Integer.parseInt(i.getStringExtra("type"));
			position = i.getIntExtra("pos", -1);
			
			//Set them to the widgets
			EditText etName = (EditText)findViewById(R.id.etName);
			etName.setText(name);
			((EditText)findViewById(R.id.etPhone)).setText(phone);
			
			((EditText)findViewById(R.id.etMail)).setText(mail);
			
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			rdg.clearCheck();
			if(phoneType == R.drawable.home){
				rdg.check(R.id.rdHome);
			}
			else{
				rdg.check(R.id.rdMobile);
			}
			
		}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			returnRecord();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void returnRecord() {
		EditText etName = (EditText)findViewById(R.id.etName);
		EditText etPhone = (EditText)findViewById(R.id.etPhone);
		RadioGroup rdgType = (RadioGroup)findViewById(R.id.rdgType);
		
		EditText etMail = (EditText)findViewById(R.id.etMail);
		
		String sName = etName.getText().toString().trim();
		String sPhone = etPhone.getText().toString().trim();
		int iType = rdgType.getCheckedRadioButtonId();
		
		String sMail = etMail.getText().toString().trim();
		
		if (sName.length() == 0 || sPhone.length() == 0 || iType == -1) {
			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle("Error");
			dialog.setMessage("All fields are required.");
			dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK", 
				new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) 
				{}});
			dialog.show();
		}
		else {
			Intent data = new Intent();
			data.putExtra("name", sName);
			data.putExtra("phone", sPhone);
			
			data.putExtra("email", sMail);
			data.putExtra("pos", position);
			
			String sType = "";
			switch(iType) {
			case R.id.rdHome:
				sType = "home";
				break;
			case R.id.rdMobile:
				sType = "mobile";
				break;
			}
			data.putExtra("type", sType);
			this.setResult(RESULT_OK, data);
			this.finish();
			Toast t = Toast.makeText(this, "A new contact added.", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}
}
